/*************************************************

Testings:

First Test it without mutex.
=> Comment out mutex lock and unlock from function increment and decrement function as below.

	void *increment(void *arg)
	{
		while(1)
		{
	//		pthread_mutex_lock(&_lock);
			printf("Inc int pre = %d\n",_int);
			++_int;
			printf("Inc int post = %d\n",_int);
	//		pthread_mutex_unlock(&_lock);
		}

	}

	void *decrement(void *arg)
	{
		while(1)
		{
	//		pthread_mutex_lock(&_lock);
			printf("Dec int pre = %d\n",_int);
			--_int;
			printf("Dec int post = %d\n",_int);
	//		pthread_mutex_unlock(&_lock);
			}
		}
	}

=> Comiple.
	cc test.c -lpthread -o test

=> Run.
	./test | tee file

=> After 1 minute close the application using CTRL+c and open the file. You can see there is missmatch into Inc and Dec count as show below.

	Inc int post = 2700
	Inc int pre = 2700
	Inc int post = 2701
	Inc int pre = 2701
	Inc int post = 2702
	Inc int pre = 2702
	Inc int post = 2703         //HERE  
	Dec int pre = 1841          //HERE
	Dec int post = 2702
	Dec int pre = 2702
	Dec int post = 2701
	Dec int pre = 2701
	Dec int post = 2700
	Dec int pre = 2700
	Dec int post = 2699
	Dec int pre = 2699

=> Repeat all the steps by uncommenting lock and unlock statements and check. Now you should not get any mismatch between inc and dec.

*/

#include <stdio.h>
#include <pthread.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

int _int;
pthread_mutex_t _lock;

void *increment(void *arg)
{
	while(1)
	{
		pthread_mutex_lock(&_lock);
		printf("Inc int pre = %d\n",_int);
		++_int;
		printf("Inc int post = %d\n",_int);
		pthread_mutex_unlock(&_lock);
	}

}

void *decrement(void *arg)
{
	while(1)
	{
		pthread_mutex_lock(&_lock);
		printf("Dec int pre = %d\n",_int);
		--_int;
		printf("Dec int post = %d\n",_int);
		pthread_mutex_unlock(&_lock);
	}
}


int main()
{
	_int = 0;

	int  status;
	pthread_t th;
	pthread_t th2;

	status=pthread_create(&th,
			NULL,
			increment,
			NULL
			);

	if(status!=0)
	{
		perror("pthread_create:");
		exit(1);
	}
	else
	{
		//When a detached thread terminates, its resources are automatically released back to the system without the need for another thread to join with the terminated thread.
		if (!pthread_detach(th))
		{
			printf("Thread detached successfully\n");
		}
		else
		{
			perror("pthread_detach:");
		}
	}


	status=pthread_create(&th2,
			NULL,
			decrement,
			NULL
			);

	if(status!=0)
	{
		perror("pthread_create:");
		exit(1);
	}
	else
	{
		//When a detached thread terminates, its resources are automatically released back to the system without the need for another thread to join with the terminated thread.
		if (!pthread_detach(th2))
		{
			printf("Thread detached successfully\n");
		}
		else
		{
			perror("pthread_detach:");
		}
	}
	while(1);

	return 0;
}